import cv2
import random
import datetime
import sys
import pathlib
import numpy as np
import os
import shutil


KINDS = {
    "classification": ("BGNF1.8", "BGNF2.1", "BK2.1", "BGNF1.0", "CS-45", "CS-10",),
    "size": ("BGNF1.8", "BGNF2.1", "BK2.1", "BGNF1.0",)
}


def select_kind_size_random(purpose):
    rand = random.Random()
    return KINDS['size'][rand.randrange(0, len(KINDS['size']))]


def select_kind_classification_random(purpose):
    rand = random.Random()
    return KINDS['classification'][rand.randrange(0, len(KINDS['classification']))]


PARTS = {
    "classification": {
        "BGNF1.8": ("DH", "DL", "PH", "PL", "F", "M"),
        "BGNF2.1": ("DH", "DL", "PH", "PL", "F", "M"),
        "BK2.1": ("DH", "DL", "PH", "PL", "F", "M"),
        "BGNF1.0": ("DH", "DL", "PH", "PL", "F", "M"),
        "CS-45": ("DD", "PD",),
        "CS-10": ("DD", "PD",),
    },
    "size": {
        "BGNF1.8": ("DH", "DL", "PH", "PL"),
        "BGNF2.1": ("DH", "DL", "PH", "PL"),
        "BK2.1": ("DH", "DL", "PH", "PL"),
        "BGNF1.0": ("DH", "DL", "PH", "PL"),
    },
}


def select_part_classification_random(kind):
    rand = random.Random()
    return PARTS['classification'][kind][rand.randrange(0, len(PARTS['classification'][kind]))]


def select_part_size_random(kind):
    rand = random.Random()
    return PARTS['size'][kind][rand.randrange(0, len(PARTS['size'][kind]))]


CODES = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)


def select_code_random():
    rand = random.Random()
    return CODES[rand.randrange(0, len(CODES))]


class GlobalDateTime:

    def __init__(self):
        self.dt = datetime.datetime.now()
        self.count = 0

    def generate(self):
        ret = self.dt + datetime.timedelta(seconds=self.count)
        self.count += 1
        return ret


global_dt = GlobalDateTime()

def generate_dt():
    return global_dt.generate()


def imwrite(filename, img, params=None):
    try:
        ext = os.path.splitext(filename)[1]
        result, n = cv2.imencode(ext, img, params)

        if result:
            with open(filename, mode='w+b') as f:
                n.tofile(f)
            return True
        else:
            return False
    except Exception as e:
        print(e)
        return False


def save_classification_bitmap(path, kind, part, code, judge, type, dt):
    bitmap = np.full(shape=(460, 460, 3), fill_value=0xcccccc, dtype=np.uint8)
    cv2.putText(bitmap, kind, (40, 80), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 0x00)
    cv2.putText(bitmap, part, (40, 160), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 0x00)
    cv2.putText(bitmap, f'{code:08d}', (40, 240), cv2.FONT_HERSHEY_SIMPLEX,
                1.0, 0x00)
    if judge:
        cv2.putText(bitmap, 'OK', (40, 320), cv2.FONT_HERSHEY_SIMPLEX,
                    1.0, 0x00)
    else:
        cv2.putText(bitmap, f'NG {type}', (40, 320), cv2.FONT_HERSHEY_SIMPLEX,
                    1.0, 0x00)
    cv2.putText(bitmap, dt.strftime("%Y%m%d%H%M%S"), (40, 400),
                cv2.FONT_HERSHEY_SIMPLEX, 1.0, 0x00)
    imwrite(str(path), bitmap)


def save_size_bitmap(path, kind, part, code, dt):
    bitmap = np.full(shape=(460, 460, 3), fill_value=0xcccccc, dtype=np.uint8)
    cv2.putText(bitmap, kind, (40, 80), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 0x00)
    cv2.putText(bitmap, part, (40, 160), cv2.FONT_HERSHEY_SIMPLEX, 1.0, 0x00)
    cv2.putText(bitmap, f'{code:08d}', (40, 240), cv2.FONT_HERSHEY_SIMPLEX,
                1.0, 0x00)
    cv2.putText(bitmap, dt.strftime("%Y%m%d%H%M%S"), (40, 320),
                cv2.FONT_HERSHEY_SIMPLEX, 1.0, 0x00)
    imwrite(str(path), bitmap)


def cleanup_dir(path: pathlib.Path):
    if not path.exists():
        return
    for file in path.glob('**/*'):
        os.remove(file)


def generate_classification_file_name(kind, part, code, judge, type, dt):
    # 命名規則：品種_バーコードNo_部位_判定_異常種類_日時.bmp
    if judge:
        return f'{kind}_{code:08d}_{part}_OK_{dt.strftime("%Y%m%d%H%M%S")}.bmp'
    else:
        return f'{kind}_{code:08d}_{part}_NG_{type}_{dt.strftime("%Y%m%d%H%M%S")}.bmp'


def generate_classification_folder_name(kind, part, code, judge):
    # 命名規則：品種_バーコードNo_部位_判定
    if judge:
        return f'{kind}_{code:08d}_{part}_OK'
    else:
        return f'{kind}_{code:08d}_{part}_NG'


def generate_size_file_name(kind, part, code, dt):
    # 命名規則：品種_バーコードNo_部位_日時.bmp
    return f'{kind}_{code:08d}_{part}_{dt.strftime("%Y%m%d%H%M%S")}.bmp'


def generate_size_folder_name(kind, part, code):
    return f'{kind}_{code:08d}_{part}_SIZE'


def generate_invalid_folder_name():
    return f'命名規則違反フォルダ'


def generate_invalide_file_name():
    return '命名規則違反ファイル.bmp'


def generate_classification(path: pathlib.Path):
    for kind in KINDS['classification']:
        for part in PARTS["classification"][kind]:
            for code in CODES:
                for judge, type in ((True, None), (False, 'IO'), (False, 'IS')):
                    outpath = path.joinpath(f'分類/{kind}_{part}/{generate_classification_folder_name(kind, part, code, judge)}')
                    outpath.mkdir(exist_ok=True, parents=True)
                    for i in range(0, 4):
                        dt = generate_dt()
                        file_name = generate_classification_file_name(kind, part, code, judge, type, dt)
                        save_classification_bitmap(outpath.joinpath(file_name), kind, part, code, judge, type, dt)


def generate_size(path: pathlib.Path):
    for kind in KINDS['size']:
        for part in PARTS['size'][kind]:
            for code in CODES:
                outpath = path.joinpath(f'サイズ/{kind}_{part}/{kind}_{code:08d}_{part}_SIZE')
                outpath.mkdir(exist_ok=True, parents=True)
                for i in range(0, 4):
                    dt = generate_dt()
                    file_name = generate_size_file_name(kind, part, code, dt)
                    save_size_bitmap(outpath.joinpath(file_name), kind, part, code, dt)


def generate_classification_size(path: pathlib.Path):
    # 分類 -> 品種・部位を１つ選択して生成
    kind_classification = select_kind_classification_random()
    part_classification = select_part_classification_random(kind_classification)
    for code in CODES:
        for judge, type in ((True, None), (False, 'IO'), (False, 'IS')):
            outpath = path.joinpath(f'分類_サイズ/{generate_classification_folder_name(kind_classification, part_classification, code, judge)}')
            cleanup_dir(outpath)
            for i in range(0, 4):
                dt = generate_dt()
                file_name = generate_classification_file_name(kind_classification, part_classification, code, judge, type, dt)
                save_classification_bitmap(outpath.joinpath(file_name), kind_classification, part_classification, code, judge, type, dt)

    # サイズ -> 品種・部位を１つ選択して生成
    kind_size = select_kind_size_random()
    part_size = select_part_size_random(kind_size)
    for code in CODES:
        outpath = path.joinpath(f'分類_サイズ/{generate_size_folder_name(kind_size, part_size, code)}')
        cleanup_dir(outpath)
        for i in range(0, 4):
            dt = generate_dt()
            file_name = generate_size_file_name(kind_size, part_size, code, dt)
            save_size_bitmap(outpath.joinpath(file_name), kind_size, part_size)


def generate_classification_multi_kinds(path: pathlib.Path):
    outpath = path.joinpath('サイズ/複数品種')


def generate_classification_multi_parts(path: pathlib.Path):
    outpath = path.joinpath('サイズ/複数部位')


def generate_size_multi_kinds(path: pathlib.Path):
    outpath = path.joinpath('分類/複数品種')


def generate_size_multi_parts(path: pathlib.Path):
    outpath = path.joinpath('分類/複数部位')


def generate_invalid_folder_name(path: pathlib.Path):
    outpath = path.joinpath(f'命名規則違反/フォルダ/{generate_invalid_folder_name()}')


def generate_invalide_file_name(path: pathlib.Path):
    outpath = path.joinpath('命名規則違反/ファイル')


def generate_invalid_kind(path: pathlib.Path):
    outpath = path.joinpath('規定されていない品種')


def generate_invalid_part(path: pathlib.Path):
    outpath = path.joinpath('規定されていない部位')


def generate_none_with_folder(path: pathlib.Path):
    kind = select_kind_classification_random()
    part = select_part_classification_random()
    code = select_code_random()
    folder_name = generate_classification_folder_name(kind, part, code, True)
    outpath = path.joinpath(f'データなし/フォルダあり/{folder_name}')
    outpath.mkdir(exist_ok=True)
    cleanup_dir(outpath)


def generate_none_without_folder(path: pathlib.Path):
    outpath = path.joinpath('データなし/フォルダなし')
    outpath.mkdir(exist_ok=True)
    cleanup_dir(outpath)


if __name__ == '__main__':
    outdir = pathlib.Path(sys.argv[0]).parent.joinpath('テストデータ')
    if outdir.exists():
        shutil.rmtree(outdir)
    outdir.mkdir(exist_ok=True)
    generate_classification(outdir)
    generate_size(outdir)
