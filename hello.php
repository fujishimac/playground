<?php
interface IHello
{
    public function func();
}

class Hello1 implements IHello
{
    public function func()
    {
        echo "hello1".PHP_EOL;
    }
}

class Hello2 implements IHello
{
    public function func()
    {
        echo "hello2".PHP_EOL;
    }
}

class Hello
{
    private IHello $value;

    function __construct(IHello $value)
    {
        $this->value = $value;
    }

    function run()
    {
        $this->value->func();
    }
}


$hello1 = new Hello(new Hello1());
$hello2 = new Hello(new Hello2());

$hello1->run();
$hello2->run();
?>
